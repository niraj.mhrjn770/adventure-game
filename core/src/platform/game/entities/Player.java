package platform.game.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import platform.game.entities.animations.player.*;
// import platform.game.music.MusicPlayer;

public class Player {
    SpriteBatch batch;

    PlayerWalk walk;
    PlayerWalkOne walk1;
    PlayerIdle idle;
    PlayerIdleOne idle1;
    PlayerAttack attack;
    PlayerAttackOne attack1;

    protected static Vector2 position = new Vector2(Gdx.graphics.getWidth() / 2, 60);

    boolean isWalking = false,
            isIdle = true,
            isRight = true,
            isAttack = false;

    private float walk_speed = 2.5f;

    public Player(SpriteBatch batch) {
        this.batch = batch;
        walk1 = new PlayerWalkOne(batch, position);
        walk = new PlayerWalk(batch, position);
        idle = new PlayerIdle(batch, position);
        idle1 = new PlayerIdleOne(batch, position);
        attack = new PlayerAttack(batch, position);
        attack1 = new PlayerAttackOne(batch, position);
    }

    private void move() {
        // movement
        if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
            isRight = true;
            isWalking = true;
            isIdle = false;
            isAttack = false;

            position.x += walk_speed;
            if (position.x > Gdx.graphics.getWidth() - 40) {
                position.x = Gdx.graphics.getWidth() - 40;
            }
        } else if (Gdx.input.isKeyPressed(Keys.LEFT)) {
            isRight = false;
            isIdle = false;
            isAttack = false;
            isWalking = true;

            position.x -= walk_speed;
            if (position.x < -25) {
                position.x = -25;
            }
        } else {
            if (Gdx.input.isKeyPressed(Keys.SPACE)) {
                isWalking = false;
                isAttack = true;
            } else {
                isAttack = false;
                isIdle = true;
                isWalking = false;
            }
        }

    }

    public void create() {
        idle.create();
        idle1.create();
        walk.create();
        walk1.create();
        attack.create();
        attack1.create();
    }

    public void render() {
        move();

        if (isWalking) {
            if (isRight)
                walk.render();
            else
                walk1.render();
        } else if (isAttack) {
            if (isRight) {
                attack.render();
            } else {
                attack1.render();
            }
        } else {
            if (isRight)
                idle.render();
            else
                idle1.render();
        }
    }

    public void dispose() {
        walk1.dispose();
        walk.dispose();
        idle.dispose();
        attack.dispose();
        attack1.dispose();
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }
}
