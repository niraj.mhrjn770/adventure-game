package platform.game.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

import platform.game.entities.animations.enemy.*;

public class Enemy {
    Vector2 position = new Vector2(Gdx.graphics.getWidth() / 4, 60);

    float walk_speed = -2.8f;

    EnemyWalk walk;
    EnemyWalkOne walkOne;
    EnemyIdle idle;
    EnemyIdleOne idleOne;

    boolean isWalking;
    boolean isIdle;
    boolean isRight;

    // public Enemy(SpriteBatch spriteBatch) {
    // this.spriteBatch = spriteBatch;
    // }
    public Enemy() {
        walkOne = new EnemyWalkOne(position);
        walk = new EnemyWalk(position);
        idle = new EnemyIdle(position);
        idleOne = new EnemyIdleOne(position);
    }

    public void create() {
        idle.create();
        idleOne.create();
        walk.create();
        walkOne.create();
    }

    private void move() {
        isWalking = true;
        isIdle = false;

        position.x += walk_speed;

        if (position.x > Gdx.graphics.getWidth() - 60 || position.x < 0)
            walk_speed = -walk_speed;

        if (position.x < 0)
            isRight = true;

        if (position.x > Gdx.graphics.getWidth() - 60)
            isRight = false;
        // System.out.println("Enemy.move(): " + isIdle);
        // System.out.println("Enemy.move(): " + position.x + " Player.move(): " + x);
    }

    public void idle() {
        // move();
        if (position.x < Player.position.x) {
            idleOne.render();
            isRight = false;
        }

        if (position.x > Player.position.x) {
            idle.render();
            isRight = true;
        }

        walk_speed = -walk_speed;
    }

    public void render() {
        move();

        if (isIdle) {
            if (isRight)
                idle.render();
            else
                idleOne.render();
        } else if (isWalking) {
            if (isRight)
                walk.render();
            else
                walkOne.render();
        }
    }

    public void dispose() {
        walk.dispose();
        walkOne.dispose();
        idle.dispose();
        idleOne.dispose();
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }
}
