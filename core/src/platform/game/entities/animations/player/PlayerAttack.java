package platform.game.entities.animations.player;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class PlayerAttack implements ApplicationListener {
    private static final int FRAME_COLS = 4, FRAME_ROWS = 1;

    Vector2 position;

    // Objects used
    SpriteBatch spriteBatch;
    Animation<TextureRegion> attackAnimation; // Must declare frame type (TextureRegion)
    Texture attackSheet;

    // A variable for tracking elapsed time for the animation
    float stateTime;

    public PlayerAttack(SpriteBatch spriteBatch, Vector2 position) {
        this.position = position;
        this.spriteBatch = spriteBatch;
    }

    @Override
    public void create() {
        // Load the sprite sheet as a Texture
        attackSheet = new Texture(Gdx.files.internal("./images/player/attack.png"));

        // Use the split utility method to create a 2D array of TextureRegions. This is
        // possible because this sprite sheet contains frames of equal size and they are
        // all aligned.
        TextureRegion[][] tmp = TextureRegion.split(attackSheet,
                attackSheet.getWidth() / FRAME_COLS,
                attackSheet.getHeight() / FRAME_ROWS);

        // Place the regions into a 1D array in the correct order, starting from the top
        // left, going across first. The Animation constructor requires a 1D array.
        TextureRegion[] attackFrame = new TextureRegion[FRAME_COLS * FRAME_ROWS];
        int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                attackFrame[index++] = tmp[i][j];
            }
        }

        // Initialize the Animation with the frame interval and array of frames
        attackAnimation = new Animation<TextureRegion>(0.085f, attackFrame);

        // Instantiate a SpriteBatch for drawing and reset the elapsed animation
        // time to 0

        stateTime = 0f;

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); // Clear screen
        stateTime += Gdx.graphics.getDeltaTime(); // Accumulate elapsed animation time

        // Get current frame of animation for the current stateTime
        TextureRegion currentFrame = attackAnimation.getKeyFrame(stateTime, true);
        // spriteBatch.begin();
        spriteBatch.draw(currentFrame, position.x, position.y);
        // spriteBatch.end();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
        attackSheet.dispose();
    }

}
