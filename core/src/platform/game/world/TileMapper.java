package platform.game.world;

import com.badlogic.gdx.graphics.OrthographicCamera;
// import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.maps.tiled.*;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class TileMapper {

    TiledMap tiledMap;
    OrthographicCamera camera = new OrthographicCamera();
    OrthogonalTiledMapRenderer render;

    public void create() {
        tiledMap = new TmxMapLoader().load("./images/world/world.tmx");
        render = new OrthogonalTiledMapRenderer(tiledMap);
        camera.setToOrtho(false);
    }

    public void render() {
        // Gdx.gl.glClearColor(0, 0, 0, 1);
        camera.update();

        render.setView(camera);
        render.render();
    }
}
