package platform.game.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import platform.game.entities.Enemy;
import platform.game.entities.Player;
import platform.game.music.MusicPlayer;

public class WorldEnvironment {
    Player player;
    Enemy enemy;

    SpriteBatch batch;
    MusicPlayer music;

    TileMapper mapper;

    public WorldEnvironment() {
    }

    public void create() {
        batch = new SpriteBatch();
        player = new Player(batch);
        enemy = new Enemy();

        mapper = new TileMapper();
        music = new MusicPlayer();

        player.create();
        enemy.create();
        mapper.create();
    }

    public void render() {
        music.play(true);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        Rectangle player_rect = new Rectangle(player.getX(), player.getY(), 20, 20);
        Rectangle enemy_rect = new Rectangle(enemy.getX(), enemy.getY(), 20, 20);

        boolean isOverlapping = enemy_rect.overlaps(player_rect);

        // Same Entity
        batch.begin();
        player.render();
        mapper.render();
        batch.end();

        // Different Entity
        if (isOverlapping)
            enemy.idle();

        else
            enemy.render();
    }

    public void dispose() {
        batch.dispose();
        enemy.dispose();
    }

}
