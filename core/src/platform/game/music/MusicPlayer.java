package platform.game.music;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.*;

public class MusicPlayer {

    Music music;
    Music hit;

    public MusicPlayer() {
        music = Gdx.audio.newMusic(Gdx.files.internal("./sounds/background_music.mp3"));
        hit = Gdx.audio.newMusic(Gdx.files.internal("./sounds/hitHurt.wav"));
    }

    public void play(boolean loop) {
        music.setLooping(loop);
        music.play();
    }

    public void hitPlay() {
        hit.setLooping(false);
        hit.play();
    }

    public void hitStop() {
        hit.stop();
    }
}
