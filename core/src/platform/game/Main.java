package platform.game;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.ScreenUtils;

import platform.game.world.WorldEnvironment;

public class Main extends ApplicationAdapter {
	// SpriteBatch batch;
	WorldEnvironment environment;

	@Override
	public void create() {
		// batch = new SpriteBatch();

		environment = new WorldEnvironment();

		environment.create();
	}

	@Override
	public void render() {
		ScreenUtils.clear(0, 0, 0, 1);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// batch.begin();
		environment.render();
		// batch.end();
	}

	@Override
	public void dispose() {
		environment.dispose();

		// batch.dispose();
	}
}
